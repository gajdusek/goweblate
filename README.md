# Golang API library for Weblate

## Status

Partly working.

not stable api, breaking changes are frequent

## Example usage

```go
package main

import (
	"fmt"

	"github.com/sirupsen/logrus"

	"gitlab.com/gajdusek/goweblate"
)

func main() {
	// Set logging level, details: https://github.com/sirupsen/logrus
	// Trace level very long and contains also api responses
	// Debug is for developers
	// Info and above are user oriented
	logrus.SetLevel(logrus.DebugLevel)
	// url to your weblate api
	// secret key for weblate user
	// if you provide secret for user without admin rights, some features will not be available depending on provided permissions
	weblate := goweblate.New("https://weblate.example.com/api/", "SECRETKEY")
	if weblate == nil {
		panic("cannot connect to weblate")
	}
	fmt.Println("Projects", weblate.GetAllProjects())
	fmt.Println("Users", weblate.GetAllUsers())
	fmt.Println("Groups", weblate.GetAllGroups())
	for _, project := range weblate.GetAllProjects() {
		fmt.Println("Components", project.GetAllComponents())
	}
}

```

## Public clases and methods

```mermaid
classDiagram
	Weblate "1" --> "*" User
	Weblate "1" --> "*" Group
	Weblate "1" --> "*" Project
	User "*" --> "*" Group
	Group "*" --> "*" User
	Project "1" --> "*" Component
	Component "1" --> "*" Translation

	class Weblate{
		GetAllUsers() []*User
		GetUser(userName string) *User

		GetAllGroups() []*Group
		GetGroup(groupID int) *Group 
		GetGroupByName(groupName string) *Group 
		GetGroupByURL(url string) (*Group, error)

		GetAllProjects() []*Project 
		GetProject(slug string) *Project
		CreateProject(name, slug, web, sourceLanguage string) (*Project, error)
		NewProject(name, slug, web, sourceLanguage string) (*Project, error)

	}
	
	class User{
		GetUserName() string
		GetFullName() string
		GetEmail() string
		GetEmail() string
		IsActive() bool
		SetActive(active bool) error
		IsSuperuser() bool
		SetSuperuser(superuser bool) error
		GetDateJoined() string 
		GetGroups() []*Group
		AddToGroup(group *Group)
	}
	
	class Group{
		GetName() string
		GetID() int
		GetUsers() []*User
		AddUser(user *User)
		AddUserByName(userName string)
	}
	
	class Project{
		GetName() string
		GetSlug() string
		GetWeb() string
		GetSourceLanguage() string

		GetAllComponents() []*Component
		GetComponent(slug string) *Component
		CreateComponent(branch, name, slug, fileFormat, filemask, repo, template, vcs, push string) (*Component, error)
		NewComponent(branch, name, slug, fileFormat, filemask, repo, template, vcs, push string) (*Component, error)
	}

	class Component{
		GetBranch() string
		GetCheckFlags() string
		IsEditTemplate() bool
		GetEnforcedChecks() string
		GetFileFormat() string
		GetFilemask() string
		GetID() int
		GetLicenseURL() string
		GetLicense() string
		GetName() string
		GetNewBase() string
		GetNewLang() string
		GetPush() string
		GetRepo() string
		GetRestricted() bool
		GetSlug() string
		GetTemplate() string
		GetVcs() string

		GetTranslation(languageCode string) *Translation
		CreateTranslation(languageCode string) (*Translation, error)
		NewTranslation(languageCode string) (*Translation, error)
	}
	class Translation{
		GetLanguageCode() string 
		GetFilename() string
		IsSource() bool
	}

```