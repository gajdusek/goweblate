package goweblate

import (
	"errors"
	"net/url"
	"path"
	"strings"

	"github.com/sirupsen/logrus"
)

// Translation struct
type Translation struct {
	component    *Component
	languageCode string
	filename     string
	isSource     bool
	units        []*Unit
}

type jsonTranslation struct {
	LanguageCode string        `json:"language_code"`
	Filename     string        `json:"filename"`
	IsSource     bool          `json:"is_source"`
	Component    jsonComponent `json:"project"`
}

type jsonPostData struct {
	Data jsonTranslation `json:"data"`
}

// GetAllTranslations return all component translations
func (c *Component) GetAllTranslations() []*Translation {
	logrus.Debug(c.slug, ".GetAllTranslations")

	if c.translations == nil {
		c.downloadTranslations()
	}
	return c.translations
}

// GetTranslation return translation from api
func (c *Component) GetTranslation(languageCode string) *Translation {
	logrus.WithFields(logrus.Fields{
		"code": languageCode,
	}).Debug(c.slug, ".GetTranslation")

	for _, t := range c.GetAllTranslations() {
		if langEqual(t.LanguageCode(), languageCode) {
			return t
		}
	}
	return nil
}

// GetTranslation return translation from api recursively, based on url e.g. https://weblate.com/api/translations/test/test/en_devel/
// 		Url format example:
//{site:https://weblate.com/api -> it is ignored}/{project:test}/{component:test}/{language code:en_devel}
func (w *Weblate) GetTranslation(targetURL string) *Translation {
	logrus.WithFields(logrus.Fields{
		"url": targetURL,
	}).Debug("GetTranslation")
	myURL, err := url.Parse(targetURL)
	if err != nil {
		logrus.Error(err)
		return nil
	}
	languageCode := path.Base(myURL.Path)
	componentURL := strings.Replace(targetURL, languageCode+"/", "", 1)
	component := w.GetComponent(componentURL)
	if component != nil {
		return component.GetTranslation(languageCode)
	}
	return nil
}

// CreateTranslation create new component from local data
func (c *Component) CreateTranslation(languageCode string) (*Translation, error) {
	logrus.WithFields(logrus.Fields{
		"languageCode": languageCode,
	}).Debug(c.slug, ".CreateTranslation")

	var jsonData jsonPostData
	postRequest := jsonTranslation{
		LanguageCode: languageCode,
	}
	resp := c.project.weblate.postRequest("components/"+c.project.slug+"/"+c.slug+"/translations/", &postRequest, &jsonData)
	if !langEqual(jsonData.Data.LanguageCode, languageCode) {
		logrus.WithFields(logrus.Fields{
			"languageCode":          languageCode,
			"jsonData.LanguageCode": jsonData.Data.LanguageCode,
			"response":              string(resp.Body()),
		}).Error("CreateTranslation: not match with response")
		return nil, errors.New("Translation language code: " + languageCode + " not match with response: " + jsonData.Data.LanguageCode + " response: " + string(resp.Body()))
	}
	logrus.WithFields(logrus.Fields{
		"project.name":   c.project.name,
		"component.name": c.name,
		"languageCode":   languageCode,
	}).Info("Created new translation")
	return c.translationFromJSON(&jsonData.Data), nil
}

// NewTranslation get translation or create if not exists
func (c *Component) NewTranslation(languageCode string) (*Translation, error) {
	logrus.WithFields(logrus.Fields{
		"languageCode": languageCode,
	}).Debug(c.name, ".NewTranslation")

	translation := c.GetTranslation(languageCode)
	if translation != nil {
		return translation, nil
	}
	return c.CreateTranslation(languageCode)
}

func (t *Translation) DeleteTranslation() error {
	logrus.Debug(t.languageCode, ".DeleteTranslation")

	resp := t.component.project.weblate.deleteRequest("translations/" + t.component.project.slug +
		"/" + t.component.slug + "/" + t.languageCode + "/")
	if resp.IsSuccess() {
		return nil
	}
	return errors.New("Fail to delete translation " + t.languageCode + " of component " + t.component.name +
		", response: " + string(resp.Body()))
}

func (c *Component) DeleteTranslation(languageCode string) error {
	logrus.WithFields(logrus.Fields{
		"languageCode": languageCode,
	}).Debug(c.slug, ".DeleteTranslation")

	translation := c.GetTranslation(languageCode)
	if translation == nil {
		return nil
	}

	return translation.DeleteTranslation()
}

// Component getter
func (t *Translation) Component() *Component {
	logrus.Trace(t.languageCode, ".Component")
	return t.component
}

// LanguageCode getter
func (t *Translation) LanguageCode() string {
	logrus.Trace(t.languageCode, ".LanguageCode")
	langCode := strings.Replace(t.languageCode, "-", "_", 1)
	return langCode
}

// Filename getter
func (t *Translation) Filename() string {
	logrus.Trace(t.languageCode, ".Filename")
	return t.filename
}

// IsSource getter
func (t *Translation) IsSource() bool {
	logrus.Trace(t.languageCode, ".IsSource")
	return t.isSource
}

// Units getter
func (t *Translation) Units() []*Unit {
	logrus.Trace(t.languageCode, ".Units")
	return t.units
}

// downloadTranslations download all translations in component and cache it
func (c *Component) downloadTranslations() {
	logrus.Trace(c.slug, ".downloadTranslations")

	type jsonTranslations struct {
		Results []*jsonTranslation
	}
	var jsonData jsonTranslations
	c.project.weblate.getRequestList("components/"+c.project.slug+"/"+c.slug+"/translations/", &jsonData)

	for _, t := range jsonData.Results {
		c.translationFromJSON(t)
	}
}

// translationFromJSON construct translation object from json
func (c *Component) translationFromJSON(jsonStruct *jsonTranslation) *Translation {
	logrus.WithFields(logrus.Fields{
		"jsonStruct.LanguageCode": jsonStruct.LanguageCode,
		"jsonStruct.Filename":     jsonStruct.Filename,
		"jsonStruct.IsSource":     jsonStruct.IsSource,
	}).Trace(c.name, ".translationFromJSON")

	translation := &Translation{
		component:    c,
		languageCode: jsonStruct.LanguageCode,
		filename:     jsonStruct.Filename,
		isSource:     jsonStruct.IsSource,
	}
	c.translations = append(c.translations, translation)
	return translation
}

func (t *Translation) String() string {
	return t.languageCode
}

// langEqual check equality of language code, it replaces - with _ hence weblate places - randomly instead of _
func langEqual(lang1, lang2 string) bool {
	return strings.Replace(lang1, "-", "_", -1) == strings.Replace(lang2, "-", "_", -1)
}
