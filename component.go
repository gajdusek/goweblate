package goweblate

import (
	"encoding/json"
	"errors"
	"net/url"
	"path"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
)

// Component struct
type Component struct {
	project                     *Project
	branch                      string
	checkFlags                  string
	allowTranslationPropagation string
	enableSuggestions           string
	editTemplate                bool
	fileFormat                  string
	filemask                    string
	id                          int
	languageRegex               string
	licenseURL                  string
	license                     string
	name                        string
	newBase                     string
	push                        string
	pushBranch                  string
	repo                        string
	restricted                  bool
	slug                        string
	sourceLanguage              string
	template                    string
	vcs                         string
	translations                []*Translation
}

type jsonResponse struct {
	AllowTranslationPropagation bool   `json:"allow_translation_propagation"`
	Branch                      string `json:"branch"`
	CheckFlags                  string `json:"check_flags"`
	EditTemplate                bool   `json:"edit_template"`
	EnableSuggestions           bool   `json:"enable_suggestions"`
	FileFormat                  string `json:"file_format"`
	Filemask                    string `json:"filemask"`
	ID                          int    `json:"id"`
	LanguageRegex               string `json:"language_regex"`
	LicenseURL                  string `json:"license_url"`
	License                     string `json:"license"`
	Name                        string `json:"name"`
	NewBase                     string `json:"new_base"`
	Push                        string `json:"push"`
	PushBranch                  string `json:"push_branch"`
	Repo                        string `json:"repo"`
	Restricted                  bool   `json:"restricted"`
	SourceLanguage              struct {
		Code string `json:"code"`
	} `json:"source_language"`
	Slug     string `json:"slug"`
	Template string `json:"template"`
	Vcs      string `json:"vcs"`
}

type jsonComponent struct {
	AllowTranslationPropagation string `json:"allow_translation_propagation"`
	Branch                      string `json:"branch"`
	CheckFlags                  string `json:"check_flags"`
	EditTemplate                bool   `json:"edit_template"`
	EnableSuggestions           string `json:"enable_suggestions"`
	FileFormat                  string `json:"file_format"`
	Filemask                    string `json:"filemask"`
	ID                          int    `json:"id"`
	LanguageRegex               string `json:"language_regex"`
	LicenseURL                  string `json:"license_url"`
	License                     string `json:"license"`
	Name                        string `json:"name"`
	NewBase                     string `json:"new_base"`
	Push                        string `json:"push"`
	PushBranch                  string `json:"push_branch"`
	Repo                        string `json:"repo"`
	Restricted                  bool   `json:"restricted"`
	SourceLanguage              struct {
		Code string `json:"code"`
	} `json:"source_language"`
	Slug     string `json:"slug"`
	Template string `json:"template"`
	Vcs      string `json:"vcs"`
}

type jsonComponent2 struct {
	CheckFlags                  string `json:"check_flags"`
	AllowTranslationPropagation string `json:"allow_translation_propagation"`
	EditTemplate                bool   `json:"edit_template"`
	EnableSuggestions           string `json:"enable_suggestions"`
	FileFormat                  string `json:"file_format"`
	Filemask                    string `json:"filemask"`
	ID                          int    `json:"id"`
	LanguageRegex               string `json:"language_regex"`
	LicenseURL                  string `json:"license_url"`
	License                     string `json:"license"`
	Name                        string `json:"name"`
	NewBase                     string `json:"new_base"`
	Repo                        string `json:"repo"`
	Restricted                  bool   `json:"restricted"`
	SourceLanguage              struct {
		Code string `json:"code"`
	} `json:"source_language"`
	Slug     string `json:"slug"`
	Template string `json:"template"`
	Vcs      string `json:"vcs"`
}

// GetAllComponents return all project components
func (p *Project) GetAllComponents() []*Component {
	logrus.Debug(p.slug, ".GetAllComponents")

	if p.components == nil {
		p.downloadComponents()
	}
	return p.components
}

// GetComponent return component from api
func (p *Project) GetComponent(slug string) *Component {
	logrus.WithFields(logrus.Fields{
		"slug": slug,
	}).Debug(p.slug, ".GetComponent")

	for _, c := range p.GetAllComponents() {
		if c.slug == slug {
			return c
		}
	}
	return nil
}

// GetComponent return component from api recursively, based on url e.g. https://weblate.com/api/translations/test/test/
// Url format: {site:https://weblate.com/api = it is ignored}/{project:test = to solve the project}/{component:test = to solve the component}/
func (w *Weblate) GetComponent(targetURL string) *Component {
	logrus.WithFields(logrus.Fields{
		"url": targetURL,
	}).Debug("GetTranslation")
	myURL, err := url.Parse(targetURL)
	if err != nil {
		logrus.Error(err)
		return nil
	}
	componentSlug := path.Base(myURL.Path)
	projectURL := strings.Replace(targetURL, componentSlug+"/", "", 1)
	projectSlug := path.Base(projectURL)
	project := w.GetProject(projectSlug)
	if project != nil {
		return project.GetComponent(componentSlug)
	}
	return nil
}

// CreateComponent create new component from local data
func (p *Project) CreateComponent(branch, name, slug, fileFormat, filemask, languageRegex, repo, template, vcs, push, pushBranch, sourceLanguage string, editTemplate bool, check_flags string, allow_translation_propagation string, enable_suggestions string) (*Component, error) {
	logrus.WithFields(logrus.Fields{
		"branch":         branch,
		"checkFlags":     check_flags,
		"name":           name,
		"slug":           slug,
		"fileFormat":     fileFormat,
		"filemask":       filemask,
		"languageRegex":  languageRegex,
		"repo":           repo,
		"template":       template,
		"vcs":            vcs,
		"push":           push,
		"pushBranch":     pushBranch,
		"sourceLanguage": sourceLanguage,
		"editTemplate":   true,
	}).Debug(p.slug, ".CreateComponent")

	if strings.Contains(repo, "weblate://") {
		var jsonData jsonResponse
		postRequest := jsonComponent2{
			AllowTranslationPropagation: allow_translation_propagation,
			CheckFlags:                  check_flags,
			EditTemplate:                editTemplate,
			EnableSuggestions:           enable_suggestions,
			FileFormat:                  fileFormat,
			Filemask:                    filemask,
			LanguageRegex:               languageRegex,
			License:                     "",
			LicenseURL:                  "",
			Name:                        name,
			Slug:                        slug,
			Repo:                        repo,
			Template:                    template,
			NewBase:                     template,
			Vcs:                         vcs,
			SourceLanguage: struct {
				Code string `json:"code"`
			}{
				Code: sourceLanguage,
			},
		}

		resp := p.weblate.postRequest("projects/"+p.slug+"/components/", &postRequest, &jsonData)
		allow_translation_propagation_bool, _ := strconv.ParseBool(allow_translation_propagation)
		if jsonData.AllowTranslationPropagation != allow_translation_propagation_bool {

			logrus.WithFields(logrus.Fields{
				"allowTranslationPropagation":          allow_translation_propagation,
				"jsonData.AllowTranslationPropagation": jsonData.AllowTranslationPropagation,
				"response":                             string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Allow translation propagation flag: " + allow_translation_propagation + " not match with response: " + strconv.FormatBool(jsonData.AllowTranslationPropagation))
		}
		enable_suggestions_bool, _ := strconv.ParseBool(enable_suggestions)
		if jsonData.EnableSuggestions != enable_suggestions_bool {

			logrus.WithFields(logrus.Fields{
				"enableSuggestions":          enable_suggestions,
				"jsonData.EnableSuggestions": jsonData.EnableSuggestions,
				"response":                   string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Enable suggestions flag: " + enable_suggestions + " not match with response: " + strconv.FormatBool(jsonData.EnableSuggestions))
		}
		if jsonData.CheckFlags != check_flags {

			logrus.WithFields(logrus.Fields{
				"checkFlags":          check_flags,
				"jsonData.CheckFlags": jsonData.CheckFlags,
				"response":            string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project check flags: " + check_flags + " not match with response: " + jsonData.CheckFlags)
		}
		if jsonData.Name != name {
			logrus.WithFields(logrus.Fields{
				"name":          name,
				"jsonData.Name": jsonData.Name,
				"response":      string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project name: " + name + " not match with response: " + jsonData.Name + " response: " + string(resp.Body()))
		}
		if jsonData.Slug != slug {
			logrus.WithFields(logrus.Fields{
				"slug":          slug,
				"jsonData.Slug": jsonData.Slug,
				"response":      string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project slug: " + slug + " not match with response: " + jsonData.Slug + " response: " + string(resp.Body()))
		}
		if jsonData.FileFormat != fileFormat {
			logrus.WithFields(logrus.Fields{
				"fileFormat":          fileFormat,
				"jsonData.FileFormat": jsonData.FileFormat,
				"response":            string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project fileFormat: " + fileFormat + " not match with response: " + jsonData.FileFormat + " response: " + string(resp.Body()))
		}
		if jsonData.Filemask != filemask {
			logrus.WithFields(logrus.Fields{
				"filemask":          filemask,
				"jsonData.Filemask": jsonData.Filemask,
				"response":          string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project filemask: " + filemask + " not match with response: " + jsonData.Filemask + " response: " + string(resp.Body()))
		}
		if jsonData.LanguageRegex != languageRegex {
			logrus.WithFields(logrus.Fields{
				"languageRegex":          languageRegex,
				"jsonData.LanguageRegex": jsonData.Filemask,
				"response":               string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project language regex: " + languageRegex + " not match with response: " + jsonData.LanguageRegex + " response: " + string(resp.Body()))
		}
		if jsonData.Template != template {
			logrus.WithFields(logrus.Fields{
				"template":          template,
				"jsonData.Template": jsonData.Template,
				"response":          string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project template: " + template + " not match with response: " + jsonData.Template + " response: " + string(resp.Body()))
		}
		if jsonData.Vcs != vcs {
			logrus.WithFields(logrus.Fields{
				"vcs":          vcs,
				"jsonData.Vcs": jsonData.Vcs,
				"response":     string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project vcs: " + vcs + " not match with response: " + jsonData.Vcs + " response: " + string(resp.Body()))
		}
		if jsonData.EditTemplate != editTemplate {
			logrus.WithFields(logrus.Fields{
				"editTemplate":         editTemplate,
				"jsonData.EditTempate": jsonData.EditTemplate,
				"response":             string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project push: " + push + " not match with response: " + jsonData.Push + " response: " + string(resp.Body()))
		}
		logrus.WithFields(logrus.Fields{
			"project.name":   p.name,
			"component.name": name,
			"component.slug": slug,
		}).Info("Created new Component")
		return p.componentFromJSON(&jsonData), nil
	} else {
		var jsonData jsonResponse
		postRequest := jsonComponent{
			AllowTranslationPropagation: allow_translation_propagation,
			Branch:                      branch,
			CheckFlags:                  check_flags,
			EditTemplate:                editTemplate,
			EnableSuggestions:           enable_suggestions,
			FileFormat:                  fileFormat,
			Filemask:                    filemask,
			LanguageRegex:               languageRegex,
			License:                     "",
			LicenseURL:                  "",
			Name:                        name,
			Slug:                        slug,
			Repo:                        repo,
			Template:                    template,
			NewBase:                     template,
			Vcs:                         vcs,
			Push:                        push,
			PushBranch:                  pushBranch,
			SourceLanguage: struct {
				Code string `json:"code"`
			}{
				Code: sourceLanguage,
			},
		}

		resp := p.weblate.postRequest("projects/"+p.slug+"/components/", &postRequest, &jsonData)
		if jsonData.Branch != branch {

			logrus.WithFields(logrus.Fields{
				"branch":          branch,
				"jsonData.Branch": jsonData.Branch,
				"response":        string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project branch: " + branch + " not match with response: " + jsonData.Branch)
		}
		allow_translation_propagation_bool, _ := strconv.ParseBool(allow_translation_propagation)
		if jsonData.AllowTranslationPropagation != allow_translation_propagation_bool {

			logrus.WithFields(logrus.Fields{
				"allowTranslationPropagation":          allow_translation_propagation,
				"jsonData.AllowTranslationPropagation": jsonData.AllowTranslationPropagation,
				"response":                             string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Allow translation propagation flag: " + allow_translation_propagation + " not match with response: " + strconv.FormatBool(jsonData.AllowTranslationPropagation))
		}
		enable_suggestions_bool, _ := strconv.ParseBool(enable_suggestions)
		if jsonData.EnableSuggestions != enable_suggestions_bool {

			logrus.WithFields(logrus.Fields{
				"enableSuggestions":          enable_suggestions,
				"jsonData.EnableSuggestions": jsonData.EnableSuggestions,
				"response":                   string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Enable suggestions flag: " + enable_suggestions + " not match with response: " + strconv.FormatBool(jsonData.EnableSuggestions))
		}
		if jsonData.CheckFlags != check_flags {

			logrus.WithFields(logrus.Fields{
				"checkFlags":          check_flags,
				"jsonData.CheckFlags": jsonData.CheckFlags,
				"response":            string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project check flags: " + check_flags + " not match with response: " + jsonData.CheckFlags)
		}
		if jsonData.Name != name {
			logrus.WithFields(logrus.Fields{
				"name":          name,
				"jsonData.Name": jsonData.Name,
				"response":      string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project name: " + name + " not match with response: " + jsonData.Name + " response: " + string(resp.Body()))
		}
		if jsonData.Slug != slug {
			logrus.WithFields(logrus.Fields{
				"slug":          slug,
				"jsonData.Slug": jsonData.Slug,
				"response":      string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project slug: " + slug + " not match with response: " + jsonData.Slug + " response: " + string(resp.Body()))
		}
		if jsonData.FileFormat != fileFormat {
			logrus.WithFields(logrus.Fields{
				"fileFormat":          fileFormat,
				"jsonData.FileFormat": jsonData.FileFormat,
				"response":            string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project fileFormat: " + fileFormat + " not match with response: " + jsonData.FileFormat + " response: " + string(resp.Body()))
		}
		if jsonData.Filemask != filemask {
			logrus.WithFields(logrus.Fields{
				"filemask":          filemask,
				"jsonData.Filemask": jsonData.Filemask,
				"response":          string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project filemask: " + filemask + " not match with response: " + jsonData.Filemask + " response: " + string(resp.Body()))
		}
		if jsonData.LanguageRegex != languageRegex {
			logrus.WithFields(logrus.Fields{
				"languageRegex":          languageRegex,
				"jsonData.LanguageRegex": jsonData.Filemask,
				"response":               string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project language regex: " + languageRegex + " not match with response: " + jsonData.LanguageRegex + " response: " + string(resp.Body()))
		}
		if jsonData.Repo != repo {
			logrus.WithFields(logrus.Fields{
				"repo":          repo,
				"jsonData.Repo": jsonData.Repo,
				"response":      string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project repo: " + repo + " not match with response: " + jsonData.Repo + " response: " + string(resp.Body()))
		}
		if jsonData.Template != template {
			logrus.WithFields(logrus.Fields{
				"template":          template,
				"jsonData.Template": jsonData.Template,
				"response":          string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project template: " + template + " not match with response: " + jsonData.Template + " response: " + string(resp.Body()))
		}
		if jsonData.Vcs != vcs {
			logrus.WithFields(logrus.Fields{
				"vcs":          vcs,
				"jsonData.Vcs": jsonData.Vcs,
				"response":     string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project vcs: " + vcs + " not match with response: " + jsonData.Vcs + " response: " + string(resp.Body()))
		}
		if jsonData.Push != push {
			logrus.WithFields(logrus.Fields{
				"push":          push,
				"jsonData.Push": jsonData.Push,
				"response":      string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project push: " + push + " not match with response: " + jsonData.Push + " response: " + string(resp.Body()))
		}
		if jsonData.EditTemplate != editTemplate {
			logrus.WithFields(logrus.Fields{
				"editTemplate":         editTemplate,
				"jsonData.EditTempate": jsonData.EditTemplate,
				"response":             string(resp.Body()),
			}).Error(p.slug, ".CreateComponent: not match with response")
			return nil, errors.New("Project push: " + push + " not match with response: " + jsonData.Push + " response: " + string(resp.Body()))
		}
		logrus.WithFields(logrus.Fields{
			"project.name":   p.name,
			"component.name": name,
			"component.slug": slug,
		}).Info("Created new Component")
		return p.componentFromJSON(&jsonData), nil
	}
}

// NewComponent get component or create if not exists
func (p *Project) NewComponent(branch, name, slug, fileFormat, filemask, languageRegex, repo, template, vcs, push, pushBranch, sourceLanguage string, editTemplate bool, check_flags string, allow_translation_propagation string, enable_suggestions string) (*Component, error) {
	logrus.WithFields(logrus.Fields{
		"branch":        branch,
		"checkFlags":    check_flags,
		"name":          name,
		"slug":          slug,
		"fileFormat":    fileFormat,
		"filemask":      filemask,
		"languageRegex": languageRegex,
		"repo":          repo,
		"template":      template,
		"vcs":           vcs,
		"push":          push,
		"pushBranch":    pushBranch,
		"editTemplate":  editTemplate,
	}).Debug(p.slug, ".NewComponent")

	if allow_translation_propagation == "" {
		allow_translation_propagation = "true"
	}

	if enable_suggestions == "" {
		enable_suggestions = "true"
	}

	component := p.GetComponent(slug)
	if component != nil {
		return component, nil
	}
	return p.CreateComponent(branch, name, slug, fileFormat, filemask, languageRegex, repo, template, vcs, push, pushBranch, sourceLanguage, editTemplate, check_flags, allow_translation_propagation, enable_suggestions)
}

// Project getter
func (c *Component) Project() *Project {
	logrus.Trace(c.slug, ".Project")
	return c.project
}

// Branch getter
func (c *Component) Branch() string {
	logrus.Trace(c.slug, ".Branch")
	return c.branch
}

// CheckFlags getter
func (c *Component) CheckFlags() string {
	logrus.Trace(c.slug, ".CheckFlags")
	return c.checkFlags
}

// AllowTranslationPropagation getter
func (c *Component) AllowTranslationPropagation() string {
	logrus.Trace(c.allowTranslationPropagation, ".AllowTranslationPropagation")
	return c.allowTranslationPropagation
}

// EnableSuggestions getter
func (c *Component) EnableSuggestions() string {
	logrus.Trace(c.enableSuggestions, ".EnableSuggestions")
	return c.enableSuggestions
}

// EditTemplate getter
func (c *Component) EditTemplate() bool {
	logrus.Trace(c.slug, ".EditTemplate")
	return c.editTemplate
}

// FileFormat getter
func (c *Component) FileFormat() string {
	logrus.Trace(c.slug, ".FileFormat")
	return c.fileFormat
}

// Filemask getter
func (c *Component) Filemask() string {
	logrus.Trace(c.slug, ".Filemask")
	return c.filemask
}

// ID getter
func (c *Component) ID() int {
	logrus.Trace(c.slug, ".ID")
	return c.id
}

// LanguageRegex getter
func (c *Component) LanguageRegex() string {
	logrus.Trace(c.slug, ".LanguageRegex")
	return c.languageRegex
}

// LicenseURL getter
func (c *Component) LicenseURL() string {
	logrus.Trace(c.slug, ".LicenseURL")
	return c.licenseURL
}

// License getter
func (c *Component) License() string {
	logrus.Trace(c.slug, ".License")
	return c.license
}

// Name getter
func (c *Component) Name() string {
	logrus.Trace(c.slug, ".Name")
	return c.name
}

// NewBase getter
func (c *Component) NewBase() string {
	logrus.Trace(c.slug, ".")
	return c.newBase
}

// Push getter
func (c *Component) Push() string {
	logrus.Trace(c.slug, ".Push")
	return c.push
}

// PushBranch getter
func (c *Component) PushBranch() string {
	logrus.Trace(c.slug, ".PushBranch")
	return c.pushBranch
}

// Repo getter
func (c *Component) Repo() string {
	logrus.Trace(c.slug, ".Repo")
	return c.repo
}

// Restricted getter
func (c *Component) Restricted() bool {
	logrus.Trace(c.slug, ".Restricted")
	return c.restricted
}

// Slug getter
func (c *Component) Slug() string {
	logrus.Trace(c.slug, ".Slug")
	return c.slug
}

// SourceLanguage getter
func (c *Component) SourceLanguage() string {
	logrus.Trace(c.slug, ".SourceLanguage")
	return c.sourceLanguage
}

// Template getter
func (c *Component) Template() string {
	logrus.Trace(c.slug, ".Template")
	return c.template
}

// Vcs getter
func (c *Component) Vcs() string {
	logrus.Trace(c.slug, ".Vcs")
	return c.vcs
}

// Translations getter
func (c *Component) Translations() []*Translation {
	logrus.Trace(c.slug, ".Translations")
	return c.translations
}

// downloadComponents download all components in projects and cache it
func (p *Project) downloadComponents() {
	logrus.Trace(p.slug, ".downloadComponents")

	type resultsStruct struct {
		Results []*jsonResponse
	}
	var jsonData resultsStruct
	p.weblate.getRequestList("projects/"+p.slug+"/components/", &jsonData)
	for _, c := range jsonData.Results {
		p.componentFromJSON(c)
	}
}

// componentFromJSON construct translation object from json
func (p *Project) componentFromJSON(jsonStruct *jsonResponse) *Component {
	logrus.WithFields(logrus.Fields{
		"jsonStruct.Repo":                jsonStruct.Repo,
		"jsonStruct.Branch":              jsonStruct.Branch,
		"jsonStruct.Filemask":            jsonStruct.Filemask,
		"jsonStruct.SourceLanguage.Code": jsonStruct.SourceLanguage.Code,
	}).Trace(p.slug, ".componentFromJSON")

	component := &Component{
		project:                     p,
		allowTranslationPropagation: strconv.FormatBool(jsonStruct.AllowTranslationPropagation),
		branch:                      jsonStruct.Branch,
		checkFlags:                  jsonStruct.CheckFlags,
		editTemplate:                jsonStruct.EditTemplate,
		enableSuggestions:           strconv.FormatBool(jsonStruct.EnableSuggestions),
		fileFormat:                  jsonStruct.FileFormat,
		filemask:                    jsonStruct.Filemask,
		id:                          jsonStruct.ID,
		languageRegex:               jsonStruct.LanguageRegex,
		licenseURL:                  jsonStruct.LicenseURL,
		license:                     jsonStruct.License,
		name:                        jsonStruct.Name,
		newBase:                     jsonStruct.NewBase,
		push:                        jsonStruct.Push,
		pushBranch:                  jsonStruct.PushBranch,
		repo:                        jsonStruct.Repo,
		restricted:                  jsonStruct.Restricted,
		slug:                        jsonStruct.Slug,
		sourceLanguage:              jsonStruct.SourceLanguage.Code,
		template:                    jsonStruct.Template,
		vcs:                         jsonStruct.Vcs,
	}
	p.components = append(p.components, component)

	return component
}

func (c *Component) String() string {
	out, _ := json.MarshalIndent(struct {
		Slug     string
		Template string
		Repo     string
	}{
		c.slug,
		c.template,
		c.repo,
	}, "", "\t")
	return string(out)
}
