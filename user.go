package goweblate

import (
	"encoding/json"
	"errors"

	"github.com/sirupsen/logrus"
)

// User struct
type User struct {
	weblate     *Weblate
	userName    string
	fullName    string
	email       string
	isSuperuser bool
	isActive    bool
	dateJoined  string
	groups      []*Group
}

type jsonUser struct {
	UserName    string `json:"username"`
	FullName    string `json:"full_name"`
	Email       string `json:"email"`
	Groups      []string
	IsSuperuser bool   `json:"is_superuser"`
	IsActive    bool   `json:"is_active"`
	DateJoined  string `json:"date_joined"`
}

// GetAllUsers GetAllUsers
func (w *Weblate) GetAllUsers() []*User {
	logrus.Debug("GetAllUsers")
	if w.users == nil {
		w.downloadUsers()
	}
	return w.users
}

// GetUser get user by userName
func (w *Weblate) GetUser(userName string) *User {
	logrus.WithFields(logrus.Fields{
		"userName": userName,
	}).Debug("GetUser")
	// find from existing
	for _, u := range w.GetAllUsers() {
		if u.userName == userName {
			return u
		}
	}
	return nil
}

// Weblate getter
func (u *User) Weblate() *Weblate {
	logrus.Trace(u.userName, ".Weblate")
	return u.weblate
}

// UserName getter
func (u *User) UserName() string {
	logrus.Trace(u.userName, ".UserName")
	return u.userName
}

// FullName getter
func (u *User) FullName() string {
	logrus.Trace(u.userName, ".FullName")
	return u.fullName
}

// Email getter
func (u *User) Email() string {
	logrus.Trace(u.userName, ".Email")
	return u.email
}

// IsActive getter
func (u *User) IsActive() bool {
	logrus.Trace(u.userName, ".IsActive")
	return u.isActive
}

// DateJoined getter
func (u *User) DateJoined() string {
	logrus.Trace(u.userName, ".DateJoined")
	return u.dateJoined
}

// Groups getter
func (u *User) Groups() []*Group {
	logrus.Trace(u.userName, ".Groups")
	return u.groups
}

// SetActive Set if user is active
func (u *User) SetActive(active bool) error {
	logrus.Debug(u.userName, ".SetActive")
	if u.isActive != active {
		var jsonData jsonUser
		putRequest := struct {
			FullName string `json:"full_name"`
			UserName string `json:"username"`
			IsActive bool   `json:"is_active"`
		}{
			FullName: u.fullName,
			UserName: u.userName,
			IsActive: active,
		}
		resp := u.weblate.putRequest("users/"+u.userName+"/", &putRequest, &jsonData)
		if jsonData.IsActive == active && jsonData.FullName == u.fullName && jsonData.UserName == u.userName {
			u.isActive = active
			logrus.WithFields(logrus.Fields{
				"userName": u.userName,
				"active":   active,
			}).Info("Enable/disable user status changed")
		} else {
			logrus.WithFields(logrus.Fields{
				"response": string(resp.Body()),
			}).Error(u.userName + ".SetActive: cannot activate/deactivate user")
			return errors.New("SetActive: cannot activate/deactivate user " + u.userName)
		}
	}
	return nil
}

// IsSuperuser IsSuperuser
func (u *User) IsSuperuser() bool {
	logrus.Trace(u.userName, ".IsSuperuser")
	return u.isSuperuser
}

// SetSuperuser Set if user is Superuser
func (u *User) SetSuperuser(superuser bool) error {
	logrus.Debug(u.userName, ".SetSuperuser")
	if u.isSuperuser != superuser {
		var jsonData jsonUser
		putRequest := struct {
			FullName    string `json:"full_name"`
			UserName    string `json:"username"`
			IsSuperuser bool   `json:"is_superuser"`
		}{
			FullName:    u.fullName,
			UserName:    u.userName,
			IsSuperuser: superuser,
		}
		resp := u.weblate.putRequest("users/"+u.userName+"/", &putRequest, &jsonData)
		if jsonData.IsSuperuser == superuser && jsonData.FullName == u.fullName && jsonData.UserName == u.userName {
			u.isSuperuser = superuser
			logrus.WithFields(logrus.Fields{
				"userName":  u.userName,
				"superuser": superuser,
			}).Info("Superuser status changed")
		} else {
			logrus.WithFields(logrus.Fields{
				"response": string(resp.Body()),
			}).Error(u.userName + ".SetSuperuser: cannot set superuser status")
			return errors.New("SetSuperuser: cannot set superuser status for user " + u.userName + " Response: " + string(resp.Body()))
		}
	}
	return nil
}

// GetDateJoined GetDateJoined
func (u *User) GetDateJoined() string {
	logrus.Trace(u.userName, ".GetDateJoined")
	return u.dateJoined
}

// GetGroups GetGroups
func (u *User) GetGroups() []*Group {
	logrus.Trace(u.userName, ".GetGroups")
	return u.groups
}

// AddToGroup Add user to group
func (u *User) AddToGroup(group *Group) error {
	logrus.WithFields(logrus.Fields{
		"group.name": group.name,
	}).Debug(u.userName, ".AddToGroup")
	for _, g := range u.groups {
		if g.id == group.id {
			// user already in group
			return nil
		}
	}
	var jsonData jsonUser
	postRequest := struct {
		GroupID int `json:"group_id"`
	}{
		GroupID: group.id,
	}
	resp := group.weblate.postRequest("users/"+u.userName+"/groups/", &postRequest, &jsonData)
	for _, g := range jsonData.Groups {
		gr, err := group.weblate.GetGroupByURL(g)
		if err != nil {
			return err
		}
		if gr.id == group.id && gr.name == group.name {
			u.groups = append(u.groups, group)
			group.users = append(group.users, u)
			logrus.WithFields(logrus.Fields{
				"user.name":  u.userName,
				"group.name": group.name,
			}).Info("User added to group")
			return nil
		}
	}
	logrus.WithFields(logrus.Fields{
		"group.name": group.name,
		"response":   string(resp.Body()),
	}).Error(u.userName + ".AddToGroup: cannot add user to group")
	return errors.New("addToGroup: cannot add user " + u.userName + " to group " + group.name)
}

func (w *Weblate) downloadUsers() {
	logrus.Trace("downloadUsers")
	if w.groups == nil {
		w.downloadGroups()
	}
	if w.users == nil {
		type resultsStruct struct {
			Results []*jsonUser
		}
		var jsonData resultsStruct
		w.getRequestList("users/", &jsonData)
		for _, u := range jsonData.Results {
			w.userFromJSON(u)
		}
	}
}

// translationFromJSON construct translation object from json
func (w *Weblate) userFromJSON(jsonStruct *jsonUser) *User {
	logrus.WithFields(logrus.Fields{
		"jsonStruct.UserName": jsonStruct.UserName,
	}).Trace("userFromJSON")
	user := &User{
		weblate:     w,
		userName:    jsonStruct.UserName,
		fullName:    jsonStruct.FullName,
		email:       jsonStruct.Email,
		isSuperuser: jsonStruct.IsSuperuser,
		isActive:    jsonStruct.IsActive,
		dateJoined:  jsonStruct.DateJoined,
	}
	for _, g := range jsonStruct.Groups {
		group, err := w.GetGroupByURL(g)
		if group != nil && err == nil {
			user.groups = append(user.groups, group)
			group.users = append(group.users, user)
		}
	}
	w.users = append(w.users, user)

	return user
}

func (u *User) String() string {
	var groups []string
	for _, g := range u.groups {
		groups = append(groups, g.name)
	}
	out, _ := json.MarshalIndent(struct {
		UserName    string
		IsActive    bool
		IsSuperuser bool
		Groups      []string
	}{
		u.userName,
		u.isActive,
		u.isSuperuser,
		groups,
	}, "", "\t")
	return string(out)
}
