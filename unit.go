package goweblate

import (
	"strconv"
	"strings"

	"encoding/json"

	"github.com/sirupsen/logrus"
)

const (
	Untranslated     = 0
	NeedsEditing     = 10
	WaitingForReview = 20
	Approved         = 30
	ReadOnly         = 100
)

// Unit struct
type Unit struct {
	translation *Translation
	id          int
	source      []string
	target      []string
	context     string
	note        string
	state       int
	explanation string
	flags       string
	extraFlags  string
	url         string
}

type jsonUnit struct {
	ID          int      `json:"id"`
	Source      []string `json:"source"`
	Target      []string `json:"target"`
	Context     string   `json:"context"`
	Note        string   `json:"note"`
	State       int      `json:"state"`
	Translation string   `json:"translation"`
	Explanation string   `json:"explanation"`
	Flags       string   `json:"flags"`
	ExtraFlags  string   `json:"extra_flags"`
	Url         string   `json:"web_url"`
}

// GetAllUnits return all units for translation
func (t *Translation) GetAllUnits() []*Unit {
	logrus.Debug(t.languageCode, ".GetAllUnits")

	if t.units == nil {
		t.downloadUnits()
	}
	return t.units
}

// GetAllUnits return all units
func (w *Weblate) GetAllUnits() []*Unit {
	logrus.Debug("GetAllUnits")
	return w.downloadUnits()
}

// GetUnit return translation from api
func (t *Translation) GetUnit(id int) *Unit {
	logrus.WithFields(logrus.Fields{
		"id": id,
	}).Debug(t.languageCode, ".GetTranslation")

	for _, u := range t.GetAllUnits() {
		if u.id == id {
			return u
		}
	}
	return nil
}

// FullUpdate unit, with limitations based on the previous state and user rights
//	0    ->   10: OK
//	10   ->   20: OK
//	20   ->   30: review workflow must be set up on the project
//	30   ->   100: can not be done -> read-only can be set only via read-only flag
//	explanation and flags can be set only on source language, use partial update on translation languages
func (u *Unit) FullUpdate(state int, target []string, explanation, extraFlags string) *Unit {
	logrus.WithFields(logrus.Fields{
		"state":       state,
		"target":      target,
		"explanation": explanation,
		"extraFlags":  extraFlags,
	}).Debug(u.id, ".FullUpdate")
	var jsonData jsonUnit
	putRequest := struct {
		State       int      `json:"state"`
		Target      []string `json:"target"`
		Explanation string   `json:"explanation"`
		Flags       string   `json:"extra_flags"`
	}{
		State:       state,
		Target:      target,
		Explanation: explanation,
		Flags:       extraFlags,
	}
	resp := u.translation.component.project.weblate.putRequest("units/"+strconv.Itoa(u.id)+"/", &putRequest, &jsonData)

	updatedUnit := u.translation.unitFromJSON(&jsonData)
	if updatedUnit.state == state {
		u.state = state
	} else {
		logrus.Warnf("State not updated on unit %d, previous state:%s, requested state:%s, unit url: %s, response: %s",
			u.id, StateDict()(u.state), StateDict()(state), u.url, string(resp.Body()))
	}
	if equal(updatedUnit.target, target) {
		u.target = target
	} else {
		logrus.Warn("Target not updated on unit ", u.id, ", previous target:", u.target,
			", requested target:", target, ", unit url: ", u.url, ", response:"+string(resp.Body()))
	}
	if updatedUnit.explanation == explanation {
		u.explanation = explanation
	} else {
		logrus.Warnf("Explanation not updated on unit %d, previous explanation:%s, requested explanation:%s, unit url: %s, response: %s",
			u.id, u.explanation, explanation, u.url, string(resp.Body()))
	}
	if updatedUnit.flags == extraFlags {
		u.flags = extraFlags
	} else {
		logrus.Warnf("Flags not updated on unit %d, previous extraFlags:%s, requested extraFlags:%s, unit url: %s, response: %s",
			u.id, u.flags, extraFlags, u.url, string(resp.Body()))
	}
	return u
}

// TranslationUpdate unit, with limitations based on the previous state and user rights
//	0    ->   10: OK
//	10   ->   20: OK
//	20   ->   30: review workflow must be set up on the project
//	30   ->   100: can not be done -> read-only can be set only via read-only flag
//	explanation and flags can be set only on source language, use full update to set them (only on source language)
func (u *Unit) TranslationUpdate(state int, target []string, extraFlags string) *Unit {
	logrus.WithFields(logrus.Fields{
		"state":      state,
		"target":     target,
		"extraFlags": extraFlags,
	}).Debug(u.id, ".TranslationUpdate")
	var jsonData jsonUnit
	patchRequest := struct {
		State  int      `json:"state"`
		Target []string `json:"target"`
		Flags  string   `json:"extra_flags"`
	}{
		State:  state,
		Target: target,
		Flags:  extraFlags,
	}
	resp := u.translation.component.project.weblate.patchRequest("units/"+strconv.Itoa(u.id)+"/", &patchRequest, &jsonData)

	updatedUnit := u.translation.unitFromJSON(&jsonData)
	if updatedUnit.state == state {
		u.state = state
	} else {
		logrus.Warnf("State not updated on unit %d, previous state:%s, requested state:%s, unit url: %s, response: %s",
			u.id, StateDict()(u.state), StateDict()(state), u.url, string(resp.Body()))
	}
	if equal(updatedUnit.target, target) {
		u.target = target
	} else {
		logrus.Warn("Target not updated on unit ", u.id, ", previous target:", u.target,
			", requested target:", target, ", unit url: ", u.url, ", response:"+string(resp.Body()))
	}
	if updatedUnit.flags == extraFlags {
		u.flags = extraFlags
	} else {
		logrus.Warnf("Flags not updated on unit %d, previous extraFlags:%s, requested extraFlags:%s, unit url: %s, response: %s",
			u.id, u.flags, extraFlags, u.url, string(resp.Body()))
	}
	return u
}

// ExtraFlagsUpdate unit
//	Explanation and flags can be set only on source language
func (u *Unit) ExtraFlagsUpdate(flags string) *Unit {
	logrus.WithFields(logrus.Fields{
		"flags": flags,
	}).Debug(u.id, ".ExtraFlagsUpdate")
	var jsonData jsonUnit
	patchRequest := struct {
		ExtraFlags string `json:"extra_flags"`
	}{
		ExtraFlags: flags,
	}
	resp := u.translation.component.project.weblate.patchRequest("units/"+strconv.Itoa(u.id)+"/", &patchRequest, &jsonData)

	updatedUnit := u.translation.unitFromJSON(&jsonData)
	if updatedUnit.extraFlags == flags {
		u.extraFlags = flags
	} else {
		logrus.Warnf("ExtraFlags not updated on unit %d, previous flags:%s, requested flags:%s, unit url: %s, response: %s",
			u.id, u.extraFlags, flags, u.url, string(resp.Body()))
	}
	return u
}

// ExplanationUpdate unit
//	Explanation and flags can be set only on source language
func (u *Unit) ExplanationUpdate(explanation string) *Unit {
	logrus.WithFields(logrus.Fields{
		"explanation": explanation,
	}).Debug(u.id, ".Explanation")
	var jsonData jsonUnit
	patchRequest := struct {
		Explanation string `json:"explanation"`
	}{
		Explanation: explanation,
	}
	resp := u.translation.component.project.weblate.patchRequest("units/"+strconv.Itoa(u.id)+"/", &patchRequest, &jsonData)

	updatedUnit := u.translation.unitFromJSON(&jsonData)
	if strings.Trim(updatedUnit.explanation, "\n") == strings.Trim(explanation, "\n") {
		u.explanation = explanation
	} else {
		logrus.Warnf("Explanation not updated on unit %d, previous explanation:%s, requested explanation:%s, unit url: %s, response: %s",
			u.id, u.explanation, explanation, u.url, string(resp.Body()))
	}
	return u
}

// Url getter
func (u *Unit) Url() string {
	logrus.Trace(u.id, ".Url")
	return u.url
}

// Flags getter
func (u *Unit) Flags() string {
	logrus.Trace(u.id, ".Flags")
	return u.flags
}

// ExtraFlags getter
func (u *Unit) ExtraFlags() string {
	logrus.Trace(u.id, ".ExtraFlags")
	return u.extraFlags
}

// Explanation getter
func (u *Unit) Explanation() string {
	logrus.Trace(u.id, ".Explanation")
	return u.explanation
}

// State getter
func (u *Unit) State() int {
	logrus.Trace(u.id, ".State")
	return u.state
}

// StateDict map integer state to the string representation of the state
//		0:   "not translated"
//		10:  "needs editing"
//		20:  "translated"
//		30:  "approved"
//		100: "read only"
func StateDict() func(int) string {
	innerMap := map[int]string{
		Untranslated:     "not translated",
		NeedsEditing:     "needs editing",
		WaitingForReview: "translated",
		Approved:         "approved",
		ReadOnly:         "read only",
	}
	return func(key int) string {
		return innerMap[key]
	}
}

// Note getter
func (u *Unit) Note() string {
	logrus.Trace(u.id, ".Note")
	return u.note
}

// Context getter
func (u *Unit) Context() string {
	logrus.Trace(u.id, ".Context")
	return u.context
}

// Target getter
func (u *Unit) Target() []string {
	logrus.Trace(u.id, ".Target")
	return u.target
}

// Source getter
func (u *Unit) Source() []string {
	logrus.Trace(u.id, ".Source")
	return u.source
}

// ID getter
func (u *Unit) ID() int {
	logrus.Trace(u.id, ".ID")
	return u.id
}

// Translation getter
func (u *Unit) Translation() *Translation {
	logrus.Trace(u.id, ".Translation")
	return u.translation
}

// downloadUnits download all units in translation and cache it
func (t *Translation) downloadUnits() {
	logrus.Trace(t.languageCode, ".downloadTranslations")

	type jsonUnits struct {
		Results []*jsonUnit
	}
	var jsonData jsonUnits
	t.component.project.weblate.getRequestList("translations/"+t.component.project.slug+"/"+t.component.slug+"/"+t.LanguageCode()+"/units/", &jsonData)

	for _, u := range jsonData.Results {
		t.unitFromJSON(u)
	}
}

// downloadUnits download all units and cache it
func (w *Weblate) downloadUnits() []*Unit {
	logrus.Trace("downloadTranslations")

	type jsonUnits struct {
		Results []*jsonUnit
	}
	var jsonData jsonUnits
	w.getRequestList("/units/", &jsonData)
	var units []*Unit
	for _, u := range jsonData.Results {
		translation := w.GetTranslation(u.Translation)
		if translation != nil {
			units = append(units, translation.unitFromJSON(u))
		}
	}
	return units
}

// translationFromJSON construct translation object from json
func (t *Translation) unitFromJSON(jsonStruct *jsonUnit) *Unit {
	logrus.WithFields(logrus.Fields{
		"jsonStruct.Context":     jsonStruct.Context,
		"jsonStruct.ID":          jsonStruct.ID,
		"jsonStruct.Note":        jsonStruct.Note,
		"jsonStruct.Source":      jsonStruct.Source,
		"jsonStruct.Target":      jsonStruct.Target,
		"jsonStruct.State":       jsonStruct.State,
		"jsonStruct.Explanation": jsonStruct.Explanation,
		"jsonStruct.Flags":       jsonStruct.Flags,
		"jsonStruct.ExtraFlags":  jsonStruct.ExtraFlags,
		"jsonStruct.Url":         jsonStruct.Url,
	}).Trace(t.languageCode, ".unitFromJSON")

	unit := &Unit{
		translation: t,
		context:     jsonStruct.Context,
		id:          jsonStruct.ID,
		note:        jsonStruct.Note,
		source:      jsonStruct.Source,
		target:      jsonStruct.Target,
		state:       jsonStruct.State,
		explanation: jsonStruct.Explanation,
		flags:       jsonStruct.Flags,
		extraFlags:  jsonStruct.ExtraFlags,
		url:         jsonStruct.Url,
	}
	t.units = append(t.units, unit)
	return unit
}

func (u *Unit) String() string {
	out, _ := json.MarshalIndent(struct {
		id          int
		Source      []string
		Target      []string
		State       string
		Note        string
		Context     string
		Explanation string
		Flags       string
		ExtraFlags  string
		Url         string
	}{
		u.id,
		u.source,
		u.target,
		StateDict()(u.state),
		u.note,
		u.context,
		u.explanation,
		u.flags,
		u.extraFlags,
		u.url,
	}, "", "\t")
	return string(out)
}

// Equal tells whether a and b contain the same elements.
// A nil argument is equivalent to an empty slice.
func equal(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}
