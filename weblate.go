package goweblate

import (
	"encoding/json"
	"regexp"
	"strconv"
	"strings"
	"time"

	resty "github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
)

// Library is statefull, but without any global variables
// everything is in object and you can instantiate it more times
// eg. if you want connect to more instances in same time
// don't instantiate it in same time more times on one url
// because it may leads to unexpected race conditions

// Because weblate have reverted users/groups model
// It's not possible to get group and all members
// but we have user and all it's group
// and for find all group members we mus iterate through all users
// we load everything into memory first
// and then we can simpler modify group members
// without iterate through users again and again

// Weblate basic library struct holds complete state
type Weblate struct {
	weblateURL string
	apiKey     string
	client     *resty.Client
	groups     []*Group
	users      []*User
	projects   []*Project
}

// New instantiate Weblate object, use one instance per one Weblate system
func New(weblateURL, apiKey string) *Weblate {
	logrus.WithFields(logrus.Fields{
		"weblateURL": weblateURL,
		"apiKey":     "hidden",
	}).Info("NewWeblate")
	weblate := Weblate{
		weblateURL: weblateURL,
		apiKey:     apiKey,
		client:     resty.New(),
	}
	type testResponse struct {
		Projects     string `json:"projects"`
		Components   string `json:"components"`
		Translations string `json:"translations"`
		Languages    string `json:"languages"`
	}
	var test testResponse
	weblate.getRequest("", &test)
	if test.Projects != "" && test.Components != "" && test.Translations != "" && test.Languages != "" {
		return &weblate
	}
	logrus.WithFields(logrus.Fields{
		"weblateURL": weblateURL,
	}).Error("Cannot connect to weblate")
	return nil
}

// pathToURL replace path to correct url
func (w *Weblate) pathToURL(path string) string {
	logrus.WithFields(logrus.Fields{
		"path": path,
	}).Trace("pathToURL")
	url := path
	if strings.HasPrefix(path, w.weblateURL) {
		url = path
	} else {
		url = w.weblateURL + path
	}
	return url
}

// getRequest request api and parse response, return it in json data
func (w *Weblate) getRequest(path string, response interface{}) *resty.Response {
	logrus.WithFields(logrus.Fields{
		"path": path,
	}).Trace("getRequest")

	resp, err := w.client.R().SetHeader("Content-Type", "application/json").SetAuthToken(w.apiKey).Get(w.pathToURL(path))
	if err != nil {
		panic(err)
	}
	if isThrottled(resp) {
		wait(resp)
		return w.getRequest(path, response)
	}
	if string(resp.Body()) == "{\"detail\":\"Not found.\"}" {
		panic("getRequest: " + path + " not found")
	} else if resp.IsError() {
		panic("getRequest: " + path + ", error code: " + strconv.Itoa(resp.StatusCode()) +
			", response: " + string(resp.Body()))
	}
	err = json.Unmarshal(resp.Body(), response)
	if err != nil {
		panic(err)
	}
	logrus.Trace("getRequest response: " + string(resp.Body()))
	return resp
}

// getRequest handle pagination and return object with one array
func (w *Weblate) getRequestList(path string, response interface{}) *resty.Response {
	logrus.WithFields(logrus.Fields{
		"path": path,
	}).Trace("getRequestList")
	type jsonList struct {
		Next    *string
		Results []*json.RawMessage
	}
	var resp *resty.Response
	var results []*json.RawMessage
	next := path
	for next != "null" {
		var jsonListData jsonList
		resp = w.getRequest(next, &jsonListData)
		if isThrottled(resp) {
			wait(resp)
			return w.getRequestList(path, response)
		}

		if resp.IsError() {
			panic("getRequest: " + next + ", error code: " + strconv.Itoa(resp.StatusCode()) +
				", response: " + string(resp.Body()))
		}

		if jsonListData.Next == nil {
			next = "null"
		} else {
			next = *jsonListData.Next
		}
		results = append(results, jsonListData.Results...)
	}
	resultsStruct := struct {
		Results []*json.RawMessage
	}{
		results,
	}
	buf, err := json.Marshal(resultsStruct)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(buf, response)
	if err != nil {
		panic(err)
	}
	return resp
}

// postRequest request api and parse response, return it in json data
func (w *Weblate) postRequest(path string, request interface{}, response interface{}) *resty.Response {
	requestByte, err := json.Marshal(request)
	if err != nil {
		panic(err)
	}
	requestString := string(requestByte)
	logrus.WithFields(logrus.Fields{
		"path": path,
		"body": requestString,
	}).Trace("postRequest")
	resp, err := w.client.R().SetHeader("Content-Type", "application/json").
		SetAuthToken(w.apiKey).SetBody(requestString).Post(w.pathToURL(path))
	if err != nil {
		panic(err)
	}
	if isThrottled(resp) {
		wait(resp)
		return w.postRequest(path, request, response)
	}
	if string(resp.Body()) == "{\"detail\":\"Not found.\"}" {
		panic("postRequest: " + path + " not found")
	} else if resp.IsError() {
		panic("postRequest: " + path + ", error code: " + strconv.Itoa(resp.StatusCode()) +
			", response: " + string(resp.Body()) + ", request: " + requestString)
	}

	err = json.Unmarshal(resp.Body(), response)
	if err != nil {
		panic(err)
	}
	logrus.Trace("postRequest response: " + string(resp.Body()))
	return resp
}

// putRequest request api and parse response, return it in json data
func (w *Weblate) putRequest(path string, request interface{}, response interface{}) *resty.Response {
	requestByte, err := json.Marshal(request)
	if err != nil {
		panic(err)
	}
	requestString := string(requestByte)
	logrus.WithFields(logrus.Fields{
		"path": path,
		"body": requestString,
	}).Trace("putRequest")
	resp, err := w.client.R().SetHeader("Content-Type", "application/json").SetAuthToken(w.apiKey).SetBody(requestString).Put(w.pathToURL(path))
	if err != nil {
		panic(err)
	}
	if isThrottled(resp) {
		wait(resp)
		return w.putRequest(path, request, response)
	}
	if string(resp.Body()) == "{\"detail\":\"Not found.\"}" {
		panic("putRequest: " + path + " not found")
	} else if resp.IsError() {
		panic("putRequest: " + path + ", error code: " + strconv.Itoa(resp.StatusCode()) +
			", response: " + string(resp.Body()) + ", request: " + requestString)
	}
	err = json.Unmarshal(resp.Body(), response)
	if err != nil {
		panic(err)
	}
	logrus.Trace("putRequest response: " + string(resp.Body()))
	return resp
}

// patchRequest request api and parse response, return it in json data
func (w *Weblate) patchRequest(path string, request interface{}, response interface{}) *resty.Response {
	requestByte, err := json.Marshal(request)
	if err != nil {
		panic(err)
	}
	requestString := string(requestByte)
	logrus.WithFields(logrus.Fields{
		"path": path,
		"body": requestString,
	}).Trace("patchRequest")
	resp, err := w.client.R().SetHeader("Content-Type", "application/json").SetAuthToken(w.apiKey).SetBody(requestString).Patch(w.pathToURL(path))
	if err != nil {
		panic(err)
	}
	if isThrottled(resp) {
		wait(resp)
		return w.patchRequest(path, request, response)
	}
	if string(resp.Body()) == "{\"detail\":\"Not found.\"}" {
		panic("patchRequest: " + path + " not found")
	} else if resp.IsError() {
		panic("patchRequest: " + path + ", error code: " + strconv.Itoa(resp.StatusCode()) +
			", response: " + string(resp.Body()) + ", request: " + requestString)
	}
	err = json.Unmarshal(resp.Body(), response)
	if err != nil {
		panic(err)
	}
	logrus.Trace("patchRequest response: " + string(resp.Body()))
	return resp
}

// deleteRequest request api
func (w *Weblate) deleteRequest(path string) *resty.Response {

	logrus.WithFields(logrus.Fields{
		"path": path,
	}).Trace("deleteRequest")
	resp, err := w.client.R().SetHeader("Content-Type", "application/json").SetAuthToken(w.apiKey).
		Delete(w.pathToURL(path))
	if err != nil {
		panic(err)
	}
	if isThrottled(resp) {
		wait(resp)
		return w.deleteRequest(path)
	}
	if resp.IsError() {
		panic("deleteRequest: " + path + ", error code: " + strconv.Itoa(resp.StatusCode()) +
			", response: " + string(resp.Body()))
	}
	logrus.Trace("deleteRequest response: " + string(resp.Body()))
	return resp
}

// Check 429: "Request was throttled. Expected available in 2246 seconds."
func isThrottled(resp *resty.Response) bool {
	return resp.StatusCode() == 429
}

// Handle 429: "Request was throttled. Expected available in 2246 seconds." - sleep for 2246 seconds etc.
func wait(resp *resty.Response) {
	reg := regexp.MustCompile("[0-9]+")
	sSecs := reg.FindString(string(resp.Body()))
	secs, _ := strconv.Atoi(sSecs)
	secs += 60
	logrus.Warnf("Waiting for server throttling for %d seconds", secs)
	time.Sleep(time.Duration(secs) * time.Second)
}
