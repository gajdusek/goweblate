package goweblate

import (
	"errors"

	"github.com/sirupsen/logrus"
)

// Addon struct
type Addon struct {
	id           int
	name         string
	componentUrl string
	url          string
	component    *Component
}

type jsonAddon struct {
	ID           int    `json:"id"`
	Name         string `json:"name"`
	ComponentUrl string `json:"component"`
	Url          string `json:"url"`
}

// Install addon for a component
func (c *Component) InstallAddon(name string) (*Addon, error) {
	logrus.WithFields(logrus.Fields{
		"name": name,
	}).Debug(c.slug, ".InstallAddon")

	var jsonData jsonAddon
	postRequest := jsonAddon{
		Name: name,
	}
	resp := c.project.weblate.postRequest("components/"+c.project.slug+"/"+c.slug+"/addons/", &postRequest, &jsonData)
	if jsonData.Name != name {
		logrus.WithFields(logrus.Fields{
			"name":          name,
			"jsonData.Name": jsonData.Name,
			"response":      string(resp.Body()),
		}).Error(c.slug, ".InstallAddon: not match with response")
		return nil, errors.New("Addon name: " + name + " not match with response: " + jsonData.Name)
	}
	logrus.WithFields(logrus.Fields{
		"project.name":   c.project.name,
		"component.name": c.name,
		"addon.name":     name,
	}).Info("Addon installed")
	return c.addonFromJSON(&jsonData), nil
}

// addonFromJSON construct addon object from json
func (c *Component) addonFromJSON(jsonStruct *jsonAddon) *Addon {
	logrus.WithFields(logrus.Fields{
		"jsonStruct.ID":        jsonStruct.ID,
		"jsonStruct.Name":      jsonStruct.Name,
		"jsonStruct.Component": jsonStruct.ComponentUrl,
		"jsonStruct.Url":       jsonStruct.Url,
	}).Trace(c.slug, ".addonFromJSON")

	addon := &Addon{
		component:    c,
		id:           jsonStruct.ID,
		name:         jsonStruct.Name,
		componentUrl: jsonStruct.ComponentUrl,
		url:          jsonStruct.Url,
	}
	return addon
}
