package goweblate

import (
	"encoding/json"
	"errors"
	"strconv"

	"github.com/sirupsen/logrus"
)

// Project struct
type Project struct {
	weblate           *Weblate
	name              string
	slug              string
	web               string
	sourceReview      bool
	translationReview bool
	components        []*Component
}

type jsonProject struct {
	Name              string `json:"name"`
	Slug              string `json:"slug"`
	Web               string `json:"web"`
	SourceReview      bool   `json:"source_review"`
	TranslationReview bool   `json:"translation_review"`
}

// GetAllProjects GetAllProjects
func (w *Weblate) GetAllProjects() []*Project {
	logrus.Debug("GetAllProjects")
	if w.projects == nil {
		w.downloadProjects()
	}
	return w.projects
}

// GetProject return project from api
func (w *Weblate) GetProject(slug string) *Project {
	logrus.WithFields(logrus.Fields{
		"slug": slug,
	}).Debug("GetProject")
	for _, p := range w.GetAllProjects() {
		if p.slug == slug {
			return p
		}
	}
	return nil
}

// CreateProject create new project from local data
func (w *Weblate) CreateProject(name, slug, web string, sourceReview, translationReview bool) (*Project, error) {
	logrus.WithFields(logrus.Fields{
		"name":              name,
		"slug":              slug,
		"web":               web,
		"sourceReview":      sourceReview,
		"translationReview": translationReview,
	}).Debug("CreateProject")
	var jsonData jsonProject
	postRequest := jsonProject{
		Name:              name,
		Slug:              slug,
		Web:               web,
		SourceReview:      sourceReview,
		TranslationReview: translationReview,
	}
	resp := w.postRequest("projects/", &postRequest, &jsonData)
	if jsonData.Name != name {
		logrus.WithFields(logrus.Fields{
			"name":          name,
			"jsonData.Name": jsonData.Name,
			"response":      string(resp.Body()),
		}).Error("CreateProject: not match with response")
		return nil, errors.New("Project name: " + name + " not match with response: " + jsonData.Name + " response: " + string(resp.Body()))
	}
	if jsonData.Slug != slug {
		logrus.WithFields(logrus.Fields{
			"slug":          slug,
			"jsonData.Slug": jsonData.Slug,
			"response":      string(resp.Body()),
		}).Error("CreateProject: not match with response")
		return nil, errors.New("Project slug: " + slug + " not match with response: " + jsonData.Slug + " response: " + string(resp.Body()))
	}
	if jsonData.Web != web {
		logrus.WithFields(logrus.Fields{
			"web":          web,
			"jsonData.Web": jsonData.Web,
			"response":     string(resp.Body()),
		}).Error("CreateProject: not match with response")
		return nil, errors.New("Project web: " + web + " not match with response: " + jsonData.Web + " response: " + string(resp.Body()))
	}
	if jsonData.Web != web {
		logrus.WithFields(logrus.Fields{
			"web":          web,
			"jsonData.Web": jsonData.Web,
			"response":     string(resp.Body()),
		}).Error("CreateProject: not match with response")
		return nil, errors.New("Project web: " + web + " not match with response: " + jsonData.Web + " response: " + string(resp.Body()))
	}
	if jsonData.SourceReview != sourceReview {
		logrus.WithFields(logrus.Fields{
			"sourceReview":          sourceReview,
			"jsonData.SourceReview": jsonData.SourceReview,
			"response":              string(resp.Body()),
		}).Error("CreateProject: not match with response")
		return nil, errors.New(
			"Project source review: " + strconv.FormatBool(sourceReview) +
				"not match with response: " + strconv.FormatBool(jsonData.SourceReview) +
				" response: " + string(resp.Body()))
	}
	if jsonData.TranslationReview != translationReview {
		logrus.WithFields(logrus.Fields{
			"translationReview":          translationReview,
			"jsonData.TranslationReview": jsonData.TranslationReview,
			"response":                   string(resp.Body()),
		}).Error("CreateProject: not match with response")
		return nil, errors.New(
			"Project translation review: " + strconv.FormatBool(translationReview) +
				"not match with response: " + strconv.FormatBool(jsonData.TranslationReview) +
				" response: " + string(resp.Body()))
	}
	logrus.WithFields(logrus.Fields{
		"name": name,
		"slug": slug,
	}).Info("Created new project")
	return w.projectFromJSON(&jsonData), nil
}

// NewProject get project or create if not exists
func (w *Weblate) NewProject(name, slug, web string, sourceReview, translationReview bool) (*Project, error) {
	logrus.WithFields(logrus.Fields{
		"name": name,
		"slug": slug,
		"web":  web,
	}).Debug("NewProject")
	project := w.GetProject(slug)
	if project != nil {
		return project, nil
	}
	return w.CreateProject(name, slug, web, sourceReview, translationReview)
}

// Weblate getter
func (p *Project) Weblate() *Weblate {
	logrus.Trace(p.slug, ".Weblate")
	return p.weblate
}

// Name getter
func (p *Project) Name() string {
	logrus.Trace(p.slug, ".Name")
	return p.name
}

// Slug getter
func (p *Project) Slug() string {
	logrus.Trace(p.slug, ".Slug")
	return p.slug
}

// Web getter
func (p *Project) Web() string {
	logrus.Trace(p.slug, ".Web")
	return p.web
}

// SourceReview getter
func (p *Project) SourceReview() bool {
	logrus.Trace(p.slug, ".SourceReview")
	return p.sourceReview
}

// TranslationReview getter
func (p *Project) TranslationReview() bool {
	logrus.Trace(p.slug, ".TranslationReview")
	return p.translationReview
}

// Components getter
func (p *Project) Components() []*Component {
	logrus.Trace(p.slug, ".Components")
	return p.components
}

// downloadProjects download all projects and cache it
func (w *Weblate) downloadProjects() {
	logrus.Trace("downloadProjects")
	type resultsStruct struct {
		Results []*jsonProject
	}
	var jsonData resultsStruct
	w.getRequestList("projects/", &jsonData)
	for _, p := range jsonData.Results {
		w.projectFromJSON(p)
	}
}

// componentFromJSON construct translation object from json
func (w *Weblate) projectFromJSON(jsonStruct *jsonProject) *Project {
	logrus.WithFields(logrus.Fields{
		"jsonStruct.Name": jsonStruct.Name,
		"jsonStruct.Slug": jsonStruct.Slug,
	}).Trace("projectFromJSON")

	project := &Project{
		weblate:           w,
		name:              jsonStruct.Name,
		slug:              jsonStruct.Slug,
		web:               jsonStruct.Web,
		sourceReview:      jsonStruct.SourceReview,
		translationReview: jsonStruct.TranslationReview,
	}
	w.projects = append(w.projects, project)

	return project
}

func (p *Project) String() string {
	out, _ := json.MarshalIndent(struct {
		Name               string
		Slug               string
		Web                string
		SourceReview       bool
		Translationreview  bool
		NumberOfComponents int
	}{
		p.name,
		p.slug,
		p.web,
		p.sourceReview,
		p.translationReview,
		len(p.GetAllComponents()),
	}, "", "\t")
	return string(out)
}
