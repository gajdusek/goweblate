package goweblate

import (
	"encoding/json"
	"errors"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
)

// Group struct
type Group struct {
	weblate *Weblate
	name    string
	id      int
	users   []*User
}

type jsonGroup struct {
	Name string
	URL  string
}

// GetAllGroups GetAllGroups
func (w *Weblate) GetAllGroups() []*Group {
	logrus.Debug("GetAllGroups")
	if w.groups == nil {
		w.downloadGroups()
	}
	return w.groups
}

// GetGroup get group by id
func (w *Weblate) GetGroup(groupID int) *Group {
	logrus.WithFields(logrus.Fields{
		"groupID": groupID,
	}).Debug("GetGroup")
	// find from existing
	for _, g := range w.GetAllGroups() {
		if g.id == groupID {
			return g
		}
	}
	return nil
}

// GetGroupByName get group by name
func (w *Weblate) GetGroupByName(groupName string) *Group {
	logrus.WithFields(logrus.Fields{
		"groupName": groupName,
	}).Debug("GetGroupByName")
	// find from existing
	for _, g := range w.GetAllGroups() {
		if g.name == groupName {
			return g
		}
	}
	return nil
}

// GetGroupByURL get group by url
func (w *Weblate) GetGroupByURL(url string) (*Group, error) {
	logrus.WithFields(logrus.Fields{
		"url": url,
	}).Debug("GetGroupByURL")
	id, err := groupIDFromURL(url)
	return w.GetGroup(id), err
}

// Weblate getter
func (g *Group) Weblate() *Weblate {
	logrus.Debug(g.name, ".Weblate")
	return g.weblate
}

// Name getter
func (g *Group) Name() string {
	logrus.Debug(g.name, ".Name")
	return g.name
}

// ID getter
func (g *Group) ID() int {
	logrus.Trace(g.name, ".ID")
	return g.id
}

// Users getter
func (g *Group) Users() []*User {
	logrus.Trace(g.name, ".Users")
	return g.users
}

// AddUser AddUser
func (g *Group) AddUser(user *User) {
	logrus.WithFields(logrus.Fields{
		"user.fullName": user.fullName,
	}).Debug(g.name, ".AddUser")
	err := user.AddToGroup(g)
	logrus.Error(err)
}

// AddUserByName AddUser by userName
func (g *Group) AddUserByName(userName string) {
	logrus.WithFields(logrus.Fields{
		"userName": userName,
	}).Debug(g.name, ".AddUserName")
	g.AddUser(g.weblate.GetUser(userName))
}

func groupIDFromURL(url string) (int, error) {
	logrus.WithFields(logrus.Fields{
		"url": url,
	}).Trace("groupIDByURL")
	url = strings.TrimSuffix(url, "/")
	url = url[strings.LastIndex(url, "/")+1:]
	id, err := strconv.Atoi(url)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"url": url,
		}).Error("Cannot get group ID from url")
		return 0, errors.New("Cannot get group ID from url: " + url)
	}
	return id, nil
}

func (w *Weblate) downloadGroups() {
	logrus.Trace("downloadGroups")
	type resultsStruct struct {
		Results []*jsonGroup
	}
	if w.groups == nil {
		var jsonData resultsStruct
		w.getRequestList("groups/", &jsonData)
		for _, g := range jsonData.Results {
			_, err := w.groupFromJSON(g)
			logrus.Error(err)
		}
	}
}

// translationFromJSON construct translation object from json
func (w *Weblate) groupFromJSON(jsonStruct *jsonGroup) (*Group, error) {
	logrus.WithFields(logrus.Fields{
		"jsonStruct.Name": jsonStruct.Name,
		"jsonStruct.URL":  jsonStruct.URL,
	}).Trace("groupFromJSON")
	id, err := groupIDFromURL(jsonStruct.URL)
	if err != nil {
		return nil, err
	}
	group := &Group{
		weblate: w,
		name:    jsonStruct.Name,
		id:      id,
	}
	w.groups = append(w.groups, group)

	return group, nil
}

func (g *Group) String() string {
	out, _ := json.MarshalIndent(struct {
		Name          string
		ID            int
		NumberOfUsers int
	}{
		g.name,
		g.id,
		len(g.users),
	}, "", "\t")
	return string(out)
}
