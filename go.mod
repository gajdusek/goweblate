require (
	github.com/go-resty/resty/v2 v2.3.0
	github.com/sirupsen/logrus v1.6.0
)

module gitlab.com/gajdusek/goweblate

go 1.14
